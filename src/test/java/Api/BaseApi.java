package Api;

import org.junit.Before;
import static io.restassured.RestAssured.baseURI;

public class BaseApi {

    @Before
    public void before() {
        baseURI = "https://reqres.in/";
    }
}
