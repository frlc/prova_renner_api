package Api;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;
import io.restassured.http.ContentType;

public class TestApi extends BaseApi {

    @Test
    public void testPostRegisterUser() {
        final String json = "{\"email\": \"eve.holt@reqres.in\", \"password\": \"pistol\"}";

        given()
            .contentType("application/json")
            .body(json)
        .when()
            .post("/api/register")
        .then()
            .statusCode(200)
            .contentType("application/json")
            .body("id", equalTo(4))
            .body("token", equalTo("QpwL5tke4Pnpja7X4"));
    }

    @Test
    public void testGetSingleUser() {
        given()
        .when()
            .get("/api/users/2")
        .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("data.first_name", equalTo("Janet"))
            .body("data.last_name", equalTo("Weaver"))
            .body("data.email", equalTo("janet.weaver@reqres.in"))
            .body("data.avatar", equalTo("https://reqres.in/img/faces/2-image.jpg"));
    }

    @Test
    public void testGetListUsers() {
        given()
        .when()
            .get("/api/users?page=2")
        .then()
            .contentType(ContentType.JSON)
            .statusCode(200)
            .body("data[0].id", equalTo(7))
            .body("data[0].first_name", equalTo("Michael"))
            .body("data[0].last_name", equalTo("Lawson"))
            .body("data[0].avatar", equalTo("https://reqres.in/img/faces/7-image.jpg"));
    }

    @Test
    public void testPatch() {
        final String json = "{\"name\": \"morpheus\", \"job\": \"zion resident\"}";

        given()
            .contentType("application/json")
            .body(json)
        .when()
            .patch("/api/users/2")
        .then()
            .statusCode(200)
            .contentType("application/json")
            .body("name", equalTo("morpheus"))
            .body("job", equalTo("zion resident"));
    }




}
